package de.viaboxx.sandbox.jsonrpc;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: <br>
 * <p>
 * Date: 10.07.15<br>
 * </p>
 */
public class Database {
    private Map<String, User> users = new HashMap<String, User>();
    private long id;

    public void saveUser(User user) {
        if (user.getId() == 0L) user.setId(++id);
        users.put(user.getUserName(), user);
    }

    public User findUserByUserName(String userName) {
        return users.get(userName);
    }

    public int getUserCount() {
        return users.size();
    }
}
