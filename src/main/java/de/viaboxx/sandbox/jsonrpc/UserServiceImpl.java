package de.viaboxx.sandbox.jsonrpc;

/**
 * Description: <br>
 * <p>
 * Date: 10.07.15<br>
 * </p>
 */
public class UserServiceImpl implements UserService {
    private Database database = new Database();
    private boolean shutdown = false;

    public User createUser(String userName, String firstName, String password) {
        User user = new User();
        user.setUserName(userName);
        user.setFirstName(firstName);
        user.setPassword(password);
        database.saveUser(user);
//        System.out.println("Server: createUser=" + user);
        return user;
    }

    public User createUser(String userName, String password) {
        return createUser(userName, null, password);
    }

    public User findUserByUserName(String userName) {
        User user = database.findUserByUserName(userName);
//        System.out.println("Server: findUserByUserName(" + userName + ")=" + user);
        return user;
    }

    public int getUserCount() {
//        System.out.println("Server: getUserCount=" + database.getUserCount());
        return database.getUserCount();
    }

    public synchronized void shutdown() {
        System.out.println("Server: shutdown");
        shutdown = true;
        notifyAll();
    }

    public boolean isRunning() {
        return !shutdown;
    }

}