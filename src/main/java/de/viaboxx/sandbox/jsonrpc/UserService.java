package de.viaboxx.sandbox.jsonrpc;

/**
 * Description: <br>
 * <p>
 * Date: 10.07.15<br>
 * </p>
 */
public interface UserService {
    User createUser(String userName, String firstName, String password);
    User createUser(String userName, String password);
    User findUserByUserName(String userName);
    int getUserCount();
    void shutdown();

    boolean isRunning();
}
