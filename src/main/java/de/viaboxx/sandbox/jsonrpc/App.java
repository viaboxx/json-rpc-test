package de.viaboxx.sandbox.jsonrpc;

import com.googlecode.jsonrpc4j.JsonRpcClient;
import com.googlecode.jsonrpc4j.JsonRpcServer;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.googlecode.jsonrpc4j.StreamServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Socket server + client test
 */
public class App {
    private static final int NUM_REQUESTS = 20000;
    public static final String HOST = "127.0.0.1";
    public static final int PORT = 1420;
    private boolean server;
    private boolean client;
    StreamServer streamServer;

    public App(String[] args) {
        if (args.length > 0 && "server".equals(args[0])) server = true;
        if (args.length > 0 && "client".equals(args[0])) client = true;
        if (args.length == 0) {
            server = true;
            client = true;
        }
    }

    public static void main(String[] args) throws Throwable {
        new App(args).start();
    }

    private void start() throws Throwable {
        if (server) server(!client);
        if (client) client();
        if (streamServer != null) {
            System.out.println("Stopping");
            streamServer.stop(); // wait for the clients to stop
        }
        System.out.println("END");
    }

    /**
     * Streaming (Socket) Server
     *
     * @throws IOException
     */
    private void server(boolean wait) throws IOException, InterruptedException {
        final UserService userService = new UserServiceImpl();
        JsonRpcServer jsonRpcServer = new JsonRpcServer(userService, UserService.class);
//        HttpServletRequest req = null;
//        HttpServletResponse resp = null;
//        jsonRpcServer.handle(req, resp);

        // create the stream server
        int maxThreads = 10;
        int maxQueuedConnections = 1;
        InetAddress bindAddress = InetAddress.getByName(HOST);
        streamServer = new StreamServer(
            jsonRpcServer, maxThreads, PORT, maxQueuedConnections, bindAddress);

        // start it, this method doesn't block
        streamServer.start();
        if (wait) {
            while (userService.isRunning()) {
                synchronized (userService) {
                    userService.wait();  // waste this thread while server is running in its own
                }
            }
        }
    }

    /**
     * Streaming client
     **/
    private void client() throws Throwable {
        Socket socket = new Socket(HOST, PORT);
        JsonRpcClient client = new JsonRpcClient();
        Thread.sleep(5000L);
        long t1 = System.currentTimeMillis();
        // HTTP client
//        JsonRpcHttpClient client = new JsonRpcHttpClient(
//            new URL("http://de.viaboxx.sandbox.jsonrpc/UserService.json"));
//        User user = client.invoke("createUser", new Object[]{"bob", "the builder"}, User.class);
//        UserService userService = ProxyUtil.createClientProxy(
//            getClass().getClassLoader(),
//            UserService.class,
//            client);

        InputStream istream = socket.getInputStream();
        OutputStream ostream = socket.getOutputStream();
        UserService userService = ProxyUtil.createClientProxy(getClass().getClassLoader(),
            UserService.class, false, client, istream, ostream);
        for (int requestNbr = 0; requestNbr < NUM_REQUESTS; requestNbr++) {
//            System.out.println("Client calls createUser " + requestNbr);
            User user = userService.createUser("bob", "the builder");
//        System.out.println("Client received " + user);
//        Thread.sleep(1000L);
//        System.out.println("Client calls findUserByUserName");
            user = userService.findUserByUserName("bob");
//        System.out.println("Client received " + user);
//        Thread.sleep(1000L);
//        System.out.println("Client calls getUserCount");
//        System.out.println("Client received " + userService.getUserCount());
        }
        long t2 = System.currentTimeMillis();
//        System.out.println("Client calls shutdown");

//        userService.shutdown();
        istream.close();
        ostream.close();
        socket.close(); // stop the client
        System.out.println("Took " + (t2 - t1) + " millis for " + NUM_REQUESTS  + " requests");
    }

}
