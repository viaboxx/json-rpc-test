package de.viaboxx.sandbox.jsonrpc;

/**
 * Description: <br>
 * <p>
 * Date: 10.07.15<br>
 * </p>
 */
public class User {
    private String firstName, userName;
    private String password;
    private long id;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
            "firstName='" + firstName + '\'' +
            ", userName='" + userName + '\'' +
            ", password='" + password + '\'' +
            ", id=" + id +
            '}';
    }
}
